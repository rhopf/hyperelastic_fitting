import sympy as sy

# variables
C1, C2, C3, p = sy.symbols('C1 C2 C3 p')
lamda, lamda1, lamda2, lamda3  = sy.symbols('lamda lamda1 lamda2 lamda3')
rho, rho0 = sy.symbols('rho rho0')
I1, I2, J = sy.symbols('I1 I2 J')
eps = sy.symbols('epsilon')

# identity
I = sy.Matrix.eye(3)

# kinematics
def set_deformation_gradient(mode):
    
    if mode == 'UA':
        # deformation gradient: uniaxial
        F = sy.Matrix([[lamda, 0, 0], 
                       [0, 1/sy.sqrt(lamda), 0], 
                       [0, 0, 1/sy.sqrt(lamda)]])
        e_vals = sy.Matrix([lamda, 1/sy.sqrt(lamda), 1/sy.sqrt(lamda)])
        E_vecs = I
    
    if mode == 'PS':
        # deformation gradient: pure shear
        F = sy.Matrix([[lamda, 0, 0], 
                       [0, 1, 0], 
                       [0, 0, 1/lamda]])
        e_vals = sy.Matrix([lamda, 1, 1/lamda])
        E_vecs = I
        
    if mode == 'EB':
        # deformation gradient: equi biaxial
        F = sy.Matrix([[lamda, 0, 0], 
                       [0, lamda, 0], 
                       [0, 0, 1/lamda**2]])
        e_vals = sy.Matrix([lamda, lamda, 1/lamda**2])
        E_vecs = I

    return F, e_vals, E_vecs

# kinematics
def set_deformation_gradient_compressible(mode):
    
    # deformation gradient: uniaxial
    F = sy.Matrix([[lamda1, 0, 0], 
                   [0, lamda2, 0], 
                   [0, 0, lamda3]])
    e_vals = sy.Matrix([lamda1, lamda2, lamda3])
    E_vecs = I

    return F, e_vals, E_vecs

def get_invariants(F):
    '''
    Returns invariants and according derivatives with respect to a given tensor F.
    '''
    
    # cauchy green deformation tensor
    C    = F.T*F
    
    # invariants
    I1 = C.trace()
    I2 = (C.trace()**2 - (C*C).trace())/2
    J  = F.det()

    # derivatives of invariants with respect to cauchy green deformation tensor
    dI1  = I
    dI2  = C.trace()*I - C
    dJ   = F.det()*C.inv()/2
    
    return I1, I2, J, dI1, dI2, dJ

def diff_principal_stretches(e_vals, E_vecs):
    '''
    Returns principal stretches and according derivatives with respect to a given tensor F.
    '''
    l1, l2, l3 = e_vals
    
    # eigenvalue tensors
    K1 = E_vecs[:, 0]*E_vecs[:, 0].T
    K2 = E_vecs[:, 1]*E_vecs[:, 1].T
    K3 = E_vecs[:, 2]*E_vecs[:, 2].T

    # derivatives of stretces with respect to cauchy green deformation tensor
    dl1  = 1/(2*l1)*K1
    dl2  = 1/(2*l2)*K2
    dl3  = 1/(2*l3)*K3
    
    return dl1, dl2, dl3

# constitutive
def get_cauchy_stress(W, F, e_vals, E_vecs, mode='invariants'):
    '''
    Calculate the Cauchy stress tensor, given the strain energy function and the deformation gradient.
    
    The `mode` variable can be either set to `invariants` or `stretches` (= for principal stretch formulation).
    '''    
    if mode == 'stretches':
        # replace J with stretches if necessary
        W = W.subs([[J, lamda1*lamda2*lamda3]])
        
        # kinematics
        l1_val, l2_val, l3_val    = e_vals
        dl1_val, dl2_val, dl3_val = diff_principal_stretches(e_vals, E_vecs)
        
        # derivatives of the strain energy functions
        dW_1 = sy.diff(W, lamda1)
        dW_2 = sy.diff(W, lamda2)
        dW_3 = sy.diff(W, lamda3)
        
        # assemble cauchy stress
        sig = 2*rho*F*(dW_1*dl1_val + dW_2*dl2_val + dW_3*dl3_val)*F.T
        
        # replace invariants
        sig = sig.subs([[lamda1, l1_val], 
                        [lamda2, l2_val], 
                        [lamda3, l3_val]])
    
    else:
        # kinematics
        I1_val, I2_val, J_val, dI1_val, dI2_val, dJ_val = get_invariants(F)
        
        # derivatives of the strain energy functions
        dW_I1  = sy.diff(W, I1)
        dW_I2  = sy.diff(W, I2)
        dW_J   = sy.diff(W, J)
        
        # assemble cauchy stress
        sig = 2*rho*F*(dW_I1*dI1_val + dW_I2*dI2_val + dW_J*dJ_val)*F.T
        
        # replace invariants
        sig = sig.subs([[I1, I1_val], 
                        [I2, I2_val], 
                        [J, J_val]])
    
    return sig

# constitutive
def get_pk1_stress(W, F, e_vals, E_vecs, mode='invariants'):
    '''
    Calculate the Piola-Kirchhoff stress tensor, given the strain energy function and the deformation gradient.
    
    The `mode` variable can be either set to `invariants` or `stretches` (= for principal stretch formulation).
    '''
    sig = get_cauchy_stress(W, F, e_vals, E_vecs, mode=mode)
    I1_val, I2_val, J_val, dI1_val, dI2_val, dJ_val = get_invariants(F)
    
    return J_val*sig*(F.inv().T)