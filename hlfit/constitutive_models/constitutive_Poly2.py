from __future__ import division
from numpy import array, exp, log, sqrt, dot
from numpy.linalg import norm

print 'Parameter order: [C10, C20, C01, C11, C21, C02, C12, C22]'

N = 8


def stress_cau(C, eps, config='UA'):

    # unpack parameters
    C10, C20, C01, C11, C21, C02, C12, C22 = C

    # set stretch
    lamda = eps + 1

    # compute stress
    if config == 'UA':
        return 2*C01*lamda**2 - 2*C01/lamda + 4*C02*lamda**4 - 12*C02*lamda**2 + 4*C02*lamda + 12*C02/lamda - 8*C02/lamda**2 + 2*C10*lamda - 2*C10/lamda**2 + 6*C11*lamda**3 - 6*C11*lamda**2 - 6*C11*lamda + 6*C11/lamda + 6*C11/lamda**2 - 6*C11/lamda**3 + 10*C12*lamda**5 - 12*C12*lamda**4 - 36*C12*lamda**3 + 54*C12*lamda**2 + 6*C12*lamda - 48*C12/lamda + 6*C12/lamda**2 + 36*C12/lamda**3 - 16*C12/lamda**4 + 8*C20*lamda**2 - 12*C20*lamda - 4*C20/lamda + 12*C20/lamda**2 - 4*C20/lamda**4 + 16*C21*lamda**4 - 36*C21*lamda**3 - 6*C21*lamda**2 + 48*C21*lamda - 6*C21/lamda - 54*C21/lamda**2 + 36*C21/lamda**3 + 12*C21/lamda**4 - 10*C21/lamda**5 + 24*C22*lamda**6 - 60*C22*lamda**5 - 60*C22*lamda**4 + 276*C22*lamda**3 - 144*C22*lamda**2 - 144*C22*lamda + 144*C22/lamda + 144*C22/lamda**2 - 276*C22/lamda**3 + 60*C22/lamda**4 + 60*C22/lamda**5 - 24*C22/lamda**6
    elif config == 'PS':
        return 2*(C01*lamda**10 - C01*lamda**6 + 2*C02*lamda**12 - 4*C02*lamda**10 + 4*C02*lamda**6 - 2*C02*lamda**4 + C10*lamda**10 - C10*lamda**6 + 2*C11*lamda**12 - 4*C11*lamda**10 + 4*C11*lamda**6 - 2*C11*lamda**4 + 3*C12*lamda**14 - 12*C12*lamda**12 + 15*C12*lamda**10 - 15*C12*lamda**6 + 12*C12*lamda**4 - 3*C12*lamda**2 + 2*C20*lamda**12 - 4*C20*lamda**10 + 4*C20*lamda**6 - 2*C20*lamda**4 + 3*C21*lamda**14 - 12*C21*lamda**12 + 15*C21*lamda**10 - 15*C21*lamda**6 + 12*C21*lamda**4 - 3*C21*lamda**2 + 4*C22*lamda**16 - 24*C22*lamda**14 + 56*C22*lamda**12 - 56*C22*lamda**10 + 56*C22*lamda**6 - 56*C22*lamda**4 + 24*C22*lamda**2 - 4*C22)/lamda**8
    elif config == 'EB':
        return 2*(C01*lamda**14 - C01*lamda**8 + 4*C02*lamda**16 - 6*C02*lamda**14 - 2*C02*lamda**10 + 6*C02*lamda**8 - 2*C02*lamda**4 + C10*lamda**16 - C10*lamda**10 + 3*C11*lamda**18 - 3*C11*lamda**16 - 3*C11*lamda**14 + 3*C11*lamda**10 + 3*C11*lamda**8 - 3*C11*lamda**6 + 8*C12*lamda**20 - 18*C12*lamda**18 - 3*C12*lamda**16 + 24*C12*lamda**14 - 3*C12*lamda**10 - 27*C12*lamda**8 + 18*C12*lamda**6 + 6*C12*lamda**4 - 5*C12*lamda**2 + 2*C20*lamda**20 - 6*C20*lamda**16 + 2*C20*lamda**14 + 6*C20*lamda**10 - 4*C20*lamda**8 + 5*C21*lamda**22 - 6*C21*lamda**20 - 18*C21*lamda**18 + 27*C21*lamda**16 + 3*C21*lamda**14 - 24*C21*lamda**10 + 3*C21*lamda**8 + 18*C21*lamda**6 - 8*C21*lamda**4 + 12*C22*lamda**24 - 30*C22*lamda**22 - 30*C22*lamda**20 + 138*C22*lamda**18 - 72*C22*lamda**16 - 72*C22*lamda**14 + 72*C22*lamda**10 + 72*C22*lamda**8 - 138*C22*lamda**6 + 30*C22*lamda**4 + 30*C22*lamda**2 - 12*C22)/lamda**12
    else:
        print 'Configuration not implemented.'


def stress_pk1(C, eps, config='UA'):

    # unpack parameters
    C10, C20, C01, C11, C21, C02, C12, C22 = C

    # set stretch
    lamda = eps + 1

    # compute stress
    if config == 'UA':
        return 2*C01*lamda - 2*C01/lamda**2 + 4*C02*lamda**3 - 12*C02*lamda + 4*C02 + 12*C02/lamda**2 - 8*C02/lamda**3 + 2*C10 - 2*C10/lamda**3 + 6*C11*lamda**2 - 6*C11*lamda - 6*C11 + 6*C11/lamda**2 + 6*C11/lamda**3 - 6*C11/lamda**4 + 10*C12*lamda**4 - 12*C12*lamda**3 - 36*C12*lamda**2 + 54*C12*lamda + 6*C12 - 48*C12/lamda**2 + 6*C12/lamda**3 + 36*C12/lamda**4 - 16*C12/lamda**5 + 8*C20*lamda - 12*C20 - 4*C20/lamda**2 + 12*C20/lamda**3 - 4*C20/lamda**5 + 16*C21*lamda**3 - 36*C21*lamda**2 - 6*C21*lamda + 48*C21 - 6*C21/lamda**2 - 54*C21/lamda**3 + 36*C21/lamda**4 + 12*C21/lamda**5 - 10*C21/lamda**6 + 24*C22*lamda**5 - 60*C22*lamda**4 - 60*C22*lamda**3 + 276*C22*lamda**2 - 144*C22*lamda - 144*C22 + 144*C22/lamda**2 + 144*C22/lamda**3 - 276*C22/lamda**4 + 60*C22/lamda**5 + 60*C22/lamda**6 - 24*C22/lamda**7
    elif config == 'PS':
        return 2*C01*lamda - 2*C01/lamda**3 + 4*C02*lamda**3 - 8*C02*lamda + 8*C02/lamda**3 - 4*C02/lamda**5 + 2*C10*lamda - 2*C10/lamda**3 + 4*C11*lamda**3 - 8*C11*lamda + 8*C11/lamda**3 - 4*C11/lamda**5 + 6*C12*lamda**5 - 24*C12*lamda**3 + 30*C12*lamda - 30*C12/lamda**3 + 24*C12/lamda**5 - 6*C12/lamda**7 + 4*C20*lamda**3 - 8*C20*lamda + 8*C20/lamda**3 - 4*C20/lamda**5 + 6*C21*lamda**5 - 24*C21*lamda**3 + 30*C21*lamda - 30*C21/lamda**3 + 24*C21/lamda**5 - 6*C21/lamda**7 + 8*C22*lamda**7 - 48*C22*lamda**5 + 112*C22*lamda**3 - 112*C22*lamda + 112*C22/lamda**3 - 112*C22/lamda**5 + 48*C22/lamda**7 - 8*C22/lamda**9
    elif config == 'EB':
        return 2*C01*lamda - 2*C01/lamda**5 + 8*C02*lamda**3 - 12*C02*lamda - 4*C02/lamda**3 + 12*C02/lamda**5 - 4*C02/lamda**9 + 2*C10*lamda**3 - 2*C10/lamda**3 + 6*C11*lamda**5 - 6*C11*lamda**3 - 6*C11*lamda + 6*C11/lamda**3 + 6*C11/lamda**5 - 6*C11/lamda**7 + 16*C12*lamda**7 - 36*C12*lamda**5 - 6*C12*lamda**3 + 48*C12*lamda - 6*C12/lamda**3 - 54*C12/lamda**5 + 36*C12/lamda**7 + 12*C12/lamda**9 - 10*C12/lamda**11 + 4*C20*lamda**7 - 12*C20*lamda**3 + 4*C20*lamda + 12*C20/lamda**3 - 8*C20/lamda**5 + 10*C21*lamda**9 - 12*C21*lamda**7 - 36*C21*lamda**5 + 54*C21*lamda**3 + 6*C21*lamda - 48*C21/lamda**3 + 6*C21/lamda**5 + 36*C21/lamda**7 - 16*C21/lamda**9 + 24*C22*lamda**11 - 60*C22*lamda**9 - 60*C22*lamda**7 + 276*C22*lamda**5 - 144*C22*lamda**3 - 144*C22*lamda + 144*C22/lamda**3 + 144*C22/lamda**5 - 276*C22/lamda**7 + 60*C22/lamda**9 + 60*C22/lamda**11 - 24*C22/lamda**13
    else:
        print 'Configuration not implemented.'


def d_stress_cau(C, eps, component, config='UA'):

    # unpack parameters
    C10, C20, C01, C11, C21, C02, C12, C22 = C

    # set stretch
    lamda = eps + 1

    # compute stress
    if component == 0:
        if config == 'UA':
            return 2*lamda - 2/lamda**2
        elif config == 'PS':
            return 2*(lamda**4 - 1)/lamda**2
        elif config == 'EB':
            return 2*(lamda**6 - 1)/lamda**2
        else:
            print 'Configuration not implemented.'

    if component == 1:
        if config == 'UA':
            return 8*lamda**2 - 12*lamda - 4/lamda + 12/lamda**2 - 4/lamda**4
        elif config == 'PS':
            return 4*(lamda**8 - 2*lamda**6 + 2*lamda**2 - 1)/lamda**4
        elif config == 'EB':
            return 4*(lamda**12 - 3*lamda**8 + lamda**6 + 3*lamda**2 - 2)/lamda**4
        else:
            print 'Configuration not implemented.'

    if component == 2:
        if config == 'UA':
            return 2*(lamda**3 - 1)/lamda
        elif config == 'PS':
            return 2*(lamda**4 - 1)/lamda**2
        elif config == 'EB':
            return 2*(lamda**6 - 1)/lamda**4
        else:
            print 'Configuration not implemented.'

    if component == 3:
        if config == 'UA':
            return 6*(lamda**4*(lamda**2 - lamda - 1) + lamda**2 + lamda - 1)/lamda**3
        elif config == 'PS':
            return 4*(lamda**8 - 2*lamda**6 + 2*lamda**2 - 1)/lamda**4
        elif config == 'EB':
            return 6*(lamda**12 - lamda**10 - lamda**8 + lamda**4 + lamda**2 - 1)/lamda**6
        else:
            print 'Configuration not implemented.'

    if component == 4:
        if config == 'UA':
            return 16*lamda**4 - 36*lamda**3 - 6*lamda**2 + 48*lamda - 6/lamda - 54/lamda**2 + 36/lamda**3 + 12/lamda**4 - 10/lamda**5
        elif config == 'PS':
            return 6*(lamda**12 - 4*lamda**10 + 5*lamda**8 - 5*lamda**4 + 4*lamda**2 - 1)/lamda**6
        elif config == 'EB':
            return 2*(5*lamda**18 - 6*lamda**16 - 18*lamda**14 + 27*lamda**12 + 3*lamda**10 - 24*lamda**6 + 3*lamda**4 + 18*lamda**2 - 8)/lamda**8
        else:
            print 'Configuration not implemented.'

    if component == 5:
        if config == 'UA':
            return 4*(lamda**3*(lamda**3 - 3*lamda + 1) + 3*lamda - 2)/lamda**2
        elif config == 'PS':
            return 4*(lamda**8 - 2*lamda**6 + 2*lamda**2 - 1)/lamda**4
        elif config == 'EB':
            return 4*(2*lamda**12 - 3*lamda**10 - lamda**6 + 3*lamda**4 - 1)/lamda**8
        else:
            print 'Configuration not implemented.'

    if component == 6:
        if config == 'UA':
            return 10*lamda**5 - 12*lamda**4 - 36*lamda**3 + 54*lamda**2 + 6*lamda - 48/lamda + 6/lamda**2 + 36/lamda**3 - 16/lamda**4
        elif config == 'PS':
            return 6*(lamda**12 - 4*lamda**10 + 5*lamda**8 - 5*lamda**4 + 4*lamda**2 - 1)/lamda**6
        elif config == 'EB':
            return 2*(8*lamda**18 - 18*lamda**16 - 3*lamda**14 + 24*lamda**12 - 3*lamda**8 - 27*lamda**6 + 18*lamda**4 + 6*lamda**2 - 5)/lamda**10
        else:
            print 'Configuration not implemented.'

    if component == 7:
        if config == 'UA':
            return 24*lamda**6 - 60*lamda**5 - 60*lamda**4 + 276*lamda**3 - 144*lamda**2 - 144*lamda + 144/lamda + 144/lamda**2 - 276/lamda**3 + 60/lamda**4 + 60/lamda**5 - 24/lamda**6
        elif config == 'PS':
            return 8*(lamda**16 - 6*lamda**14 + 14*lamda**12 - 14*lamda**10 + 14*lamda**6 - 14*lamda**4 + 6*lamda**2 - 1)/lamda**8
        elif config == 'EB':
            return 12*(2*lamda**24 - 5*lamda**22 - 5*lamda**20 + 23*lamda**18 - 12*lamda**16 - 12*lamda**14 + 12*lamda**10 + 12*lamda**8 - 23*lamda**6 + 5*lamda**4 + 5*lamda**2 - 2)/lamda**12
        else:
            print 'Configuration not implemented.'

    print 'Error.'


def d_stress_pk1(C, eps, component, config='UA'):

    # unpack parameters
    C10, C20, C01, C11, C21, C02, C12, C22 = C

    # set stretch
    lamda = eps + 1

    # compute stress
    if component == 0:
        if config == 'UA':
            return 2 - 2/lamda**3
        elif config == 'PS':
            return 2*lamda - 2/lamda**3
        elif config == 'EB':
            return 2*(lamda**6 - 1)/lamda**3
        else:
            print 'Configuration not implemented.'

    if component == 1:
        if config == 'UA':
            return 8*lamda - 12 - 4/lamda**2 + 12/lamda**3 - 4/lamda**5
        elif config == 'PS':
            return 4*lamda**3 - 8*lamda + 8/lamda**3 - 4/lamda**5
        elif config == 'EB':
            return 4*lamda**7 - 12*lamda**3 + 4*lamda + 12/lamda**3 - 8/lamda**5
        else:
            print 'Configuration not implemented.'

    if component == 2:
        if config == 'UA':
            return 2*lamda - 2/lamda**2
        elif config == 'PS':
            return 2*lamda - 2/lamda**3
        elif config == 'EB':
            return 2*lamda - 2/lamda**5
        else:
            print 'Configuration not implemented.'

    if component == 3:
        if config == 'UA':
            return 6*(lamda**4*(lamda**2 - lamda - 1) + lamda**2 + lamda - 1)/lamda**4
        elif config == 'PS':
            return 4*lamda**3 - 8*lamda + 8/lamda**3 - 4/lamda**5
        elif config == 'EB':
            return 6*(lamda**8*(lamda**4 - lamda**2 - 1) + lamda**4 + lamda**2 - 1)/lamda**7
        else:
            print 'Configuration not implemented.'

    if component == 4:
        if config == 'UA':
            return 16*lamda**3 - 36*lamda**2 - 6*lamda + 48 - 6/lamda**2 - 54/lamda**3 + 36/lamda**4 + 12/lamda**5 - 10/lamda**6
        elif config == 'PS':
            return 6*lamda**5 - 24*lamda**3 + 30*lamda - 30/lamda**3 + 24/lamda**5 - 6/lamda**7
        elif config == 'EB':
            return 10*lamda**9 - 12*lamda**7 - 36*lamda**5 + 54*lamda**3 + 6*lamda - 48/lamda**3 + 6/lamda**5 + 36/lamda**7 - 16/lamda**9
        else:
            print 'Configuration not implemented.'

    if component == 5:
        if config == 'UA':
            return 4*lamda**3 - 12*lamda + 4 + 12/lamda**2 - 8/lamda**3
        elif config == 'PS':
            return 4*lamda**3 - 8*lamda + 8/lamda**3 - 4/lamda**5
        elif config == 'EB':
            return 8*lamda**3 - 12*lamda - 4/lamda**3 + 12/lamda**5 - 4/lamda**9
        else:
            print 'Configuration not implemented.'

    if component == 6:
        if config == 'UA':
            return 10*lamda**4 - 12*lamda**3 - 36*lamda**2 + 54*lamda + 6 - 48/lamda**2 + 6/lamda**3 + 36/lamda**4 - 16/lamda**5
        elif config == 'PS':
            return 6*lamda**5 - 24*lamda**3 + 30*lamda - 30/lamda**3 + 24/lamda**5 - 6/lamda**7
        elif config == 'EB':
            return 16*lamda**7 - 36*lamda**5 - 6*lamda**3 + 48*lamda - 6/lamda**3 - 54/lamda**5 + 36/lamda**7 + 12/lamda**9 - 10/lamda**11
        else:
            print 'Configuration not implemented.'

    if component == 7:
        if config == 'UA':
            return 24*lamda**5 - 60*lamda**4 - 60*lamda**3 + 276*lamda**2 - 144*lamda - 144 + 144/lamda**2 + 144/lamda**3 - 276/lamda**4 + 60/lamda**5 + 60/lamda**6 - 24/lamda**7
        elif config == 'PS':
            return 8*lamda**7 - 48*lamda**5 + 112*lamda**3 - 112*lamda + 112/lamda**3 - 112/lamda**5 + 48/lamda**7 - 8/lamda**9
        elif config == 'EB':
            return 24*lamda**11 - 60*lamda**9 - 60*lamda**7 + 276*lamda**5 - 144*lamda**3 - 144*lamda + 144/lamda**3 + 144/lamda**5 - 276/lamda**7 + 60/lamda**9 + 60/lamda**11 - 24/lamda**13
        else:
            print 'Configuration not implemented.'

    print 'Error.'


def objective_function_cau(C, eps_exp, sig_exp, configs, w):

    # assertion for weights
    assert len(w) == 3, 'Set weights of unused configurations to zero!'

    # unpack parameters
    C10, C20, C01, C11, C21, C02, C12, C22 = C

    psi_ua = 0
    psi_ps = 0
    psi_eb = 0

    # set up target function
    if 'UA' in configs:
        psi_ua = stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA']
        psi_ua = (w[0]**2)*dot(psi_ua, psi_ua)

    if 'PS' in configs:
        psi_ps = stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS']
        psi_ps = (w[1]**2)*dot(psi_ps, psi_ps)

    if 'EB' in configs:
        psi_eb = stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB']
        psi_eb = (w[2]**2)*dot(psi_eb, psi_eb)

    # sum targets
    psi = psi_ua + psi_ps + psi_eb

    return psi


def objective_function_pk1(C, eps_exp, sig_exp, configs, w):

    # assertion for weights
    assert len(w) == 3, 'Set weights of unused configurations to zero!'

    # unpack parameters
    C10, C20, C01, C11, C21, C02, C12, C22 = C

    psi_ua = 0
    psi_ps = 0
    psi_eb = 0

    # set up target function
    if 'UA' in configs:
        psi_ua = stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA']
        psi_ua = (w[0]**2)*dot(psi_ua, psi_ua)

    if 'PS' in configs:
        psi_ps = stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS']
        psi_ps = (w[1]**2)*dot(psi_ps, psi_ps)

    if 'EB' in configs:
        psi_eb = stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB']
        psi_eb = (w[2]**2)*dot(psi_eb, psi_eb)

    # sum targets
    psi = psi_ua + psi_ps + psi_eb

    return psi


def jacobian_cau(C, eps_exp, sig_exp, configs, w):

    # assertion for weights
    assert len(w) == 3, 'Set weights w = [..., ..., ...] of unused configurations to zero!'

    # unpack parameters
    C10, C20, C01, C11, C21, C02, C12, C22 = C

    # set up jacobian
    jac = list()
    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_cau(C, eps_exp['UA'], 0, config='UA'), stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_cau(C, eps_exp['PS'], 0, config='PS'), stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_cau(C, eps_exp['EB'], 0, config='EB'), stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_cau(C, eps_exp['UA'], 1, config='UA'), stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_cau(C, eps_exp['PS'], 1, config='PS'), stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_cau(C, eps_exp['EB'], 1, config='EB'), stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_cau(C, eps_exp['UA'], 2, config='UA'), stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_cau(C, eps_exp['PS'], 2, config='PS'), stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_cau(C, eps_exp['EB'], 2, config='EB'), stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_cau(C, eps_exp['UA'], 3, config='UA'), stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_cau(C, eps_exp['PS'], 3, config='PS'), stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_cau(C, eps_exp['EB'], 3, config='EB'), stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_cau(C, eps_exp['UA'], 4, config='UA'), stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_cau(C, eps_exp['PS'], 4, config='PS'), stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_cau(C, eps_exp['EB'], 4, config='EB'), stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_cau(C, eps_exp['UA'], 5, config='UA'), stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_cau(C, eps_exp['PS'], 5, config='PS'), stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_cau(C, eps_exp['EB'], 5, config='EB'), stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_cau(C, eps_exp['UA'], 6, config='UA'), stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_cau(C, eps_exp['PS'], 6, config='PS'), stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_cau(C, eps_exp['EB'], 6, config='EB'), stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_cau(C, eps_exp['UA'], 7, config='UA'), stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_cau(C, eps_exp['PS'], 7, config='PS'), stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_cau(C, eps_exp['EB'], 7, config='EB'), stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    return array(jac)


def jacobian_pk1(C, eps_exp, sig_exp, configs, w):

    # assertion for weights
    assert len(w) == 3, 'Set weights w = [..., ..., ...] of unused configurations to zero!'

    # unpack parameters
    C10, C20, C01, C11, C21, C02, C12, C22 = C

    # set up jacobian
    jac = list()
    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_pk1(C, eps_exp['UA'], 0, config='UA'), stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_pk1(C, eps_exp['PS'], 0, config='PS'), stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_pk1(C, eps_exp['EB'], 0, config='EB'), stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_pk1(C, eps_exp['UA'], 1, config='UA'), stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_pk1(C, eps_exp['PS'], 1, config='PS'), stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_pk1(C, eps_exp['EB'], 1, config='EB'), stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_pk1(C, eps_exp['UA'], 2, config='UA'), stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_pk1(C, eps_exp['PS'], 2, config='PS'), stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_pk1(C, eps_exp['EB'], 2, config='EB'), stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_pk1(C, eps_exp['UA'], 3, config='UA'), stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_pk1(C, eps_exp['PS'], 3, config='PS'), stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_pk1(C, eps_exp['EB'], 3, config='EB'), stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_pk1(C, eps_exp['UA'], 4, config='UA'), stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_pk1(C, eps_exp['PS'], 4, config='PS'), stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_pk1(C, eps_exp['EB'], 4, config='EB'), stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_pk1(C, eps_exp['UA'], 5, config='UA'), stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_pk1(C, eps_exp['PS'], 5, config='PS'), stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_pk1(C, eps_exp['EB'], 5, config='EB'), stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_pk1(C, eps_exp['UA'], 6, config='UA'), stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_pk1(C, eps_exp['PS'], 6, config='PS'), stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_pk1(C, eps_exp['EB'], 6, config='EB'), stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_pk1(C, eps_exp['UA'], 7, config='UA'), stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_pk1(C, eps_exp['PS'], 7, config='PS'), stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_pk1(C, eps_exp['EB'], 7, config='EB'), stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    return array(jac)
