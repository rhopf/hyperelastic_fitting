from __future__ import division
from numpy import array, exp, log, sqrt, dot
from numpy.linalg import norm

print 'Parameter order: [mu0, q, m2]'

N = 3


def stress_cau(C, eps, config='UA'):

    # unpack parameters
    mu0, q, m2 = C

    # set stretch
    lamda = eps + 1

    # compute stress
    if config == 'UA':
        return m2*mu0*(lamda**3 - 1)*exp(m2*q*(lamda*(lamda**2 - 3) + 2)/lamda)/lamda
    elif config == 'PS':
        return m2*mu0*(lamda**4 - 1)*exp(m2*q*(lamda**2*(lamda**2 - 2) + 1)/lamda**2)/lamda**2
    elif config == 'EB':
        return m2*mu0*(lamda**6 - 1)*exp(m2*q*(lamda**4*(2*lamda**2 - 3) + 1)/lamda**4)/lamda**4
    else:
        print 'Configuration not implemented.'


def stress_pk1(C, eps, config='UA'):

    # unpack parameters
    mu0, q, m2 = C

    # set stretch
    lamda = eps + 1

    # compute stress
    if config == 'UA':
        return m2*mu0*(lamda**3 - 1)*exp(m2*q*(lamda*(lamda**2 - 3) + 2)/lamda)/lamda**2
    elif config == 'PS':
        return m2*mu0*(lamda**4 - 1)*exp(m2*q*(lamda**2*(lamda**2 - 2) + 1)/lamda**2)/lamda**3
    elif config == 'EB':
        return m2*mu0*(lamda**6 - 1)*exp(m2*q*(lamda**4*(2*lamda**2 - 3) + 1)/lamda**4)/lamda**5
    else:
        print 'Configuration not implemented.'


def d_stress_cau(C, eps, component, config='UA'):

    # unpack parameters
    mu0, q, m2 = C

    # set stretch
    lamda = eps + 1

    # compute stress
    if component == 0:
        if config == 'UA':
            return m2*(lamda**3 - 1)*exp(m2*q*(lamda*(lamda**2 - 3) + 2)/lamda)/lamda
        elif config == 'PS':
            return m2*(lamda**4 - 1)*exp(m2*q*(lamda**2*(lamda**2 - 2) + 1)/lamda**2)/lamda**2
        elif config == 'EB':
            return m2*(lamda**6 - 1)*exp(m2*q*(lamda**4*(2*lamda**2 - 3) + 1)/lamda**4)/lamda**4
        else:
            print 'Configuration not implemented.'

    if component == 1:
        if config == 'UA':
            return m2**2*mu0*(lamda**3 - 1)*(lamda*(lamda**2 - 3) + 2)*exp(m2*q*(lamda*(lamda**2 - 3) + 2)/lamda)/lamda**2
        elif config == 'PS':
            return m2**2*mu0*(lamda**4 - 1)*(lamda**2*(lamda**2 - 2) + 1)*exp(m2*q*(lamda**2*(lamda**2 - 2) + 1)/lamda**2)/lamda**4
        elif config == 'EB':
            return m2**2*mu0*(lamda**6 - 1)*(lamda**4*(2*lamda**2 - 3) + 1)*exp(m2*q*(lamda**4*(2*lamda**2 - 3) + 1)/lamda**4)/lamda**8
        else:
            print 'Configuration not implemented.'

    if component == 2:
        if config == 'UA':
            return mu0*(lamda + m2*q*(lamda*(lamda**2 - 3) + 2))*(lamda**3 - 1)*exp(m2*q*(lamda*(lamda**2 - 3) + 2)/lamda)/lamda**2
        elif config == 'PS':
            return mu0*(lamda**2 + m2*q*(lamda**2*(lamda**2 - 2) + 1))*(lamda**4 - 1)*exp(m2*q*(lamda**2*(lamda**2 - 2) + 1)/lamda**2)/lamda**4
        elif config == 'EB':
            return mu0*(lamda**4 + m2*q*(lamda**4*(2*lamda**2 - 3) + 1))*(lamda**6 - 1)*exp(m2*q*(lamda**4*(2*lamda**2 - 3) + 1)/lamda**4)/lamda**8
        else:
            print 'Configuration not implemented.'

    print 'Error.'


def d_stress_pk1(C, eps, component, config='UA'):

    # unpack parameters
    mu0, q, m2 = C

    # set stretch
    lamda = eps + 1

    # compute stress
    if component == 0:
        if config == 'UA':
            return m2*(lamda**3 - 1)*exp(m2*q*(lamda*(lamda**2 - 3) + 2)/lamda)/lamda**2
        elif config == 'PS':
            return m2*(lamda**4 - 1)*exp(m2*q*(lamda**2*(lamda**2 - 2) + 1)/lamda**2)/lamda**3
        elif config == 'EB':
            return m2*(lamda**6 - 1)*exp(m2*q*(lamda**4*(2*lamda**2 - 3) + 1)/lamda**4)/lamda**5
        else:
            print 'Configuration not implemented.'

    if component == 1:
        if config == 'UA':
            return m2**2*mu0*(lamda**3 - 1)*(lamda*(lamda**2 - 3) + 2)*exp(m2*q*(lamda*(lamda**2 - 3) + 2)/lamda)/lamda**3
        elif config == 'PS':
            return m2**2*mu0*(lamda**4 - 1)*(lamda**2*(lamda**2 - 2) + 1)*exp(m2*q*(lamda**2*(lamda**2 - 2) + 1)/lamda**2)/lamda**5
        elif config == 'EB':
            return m2**2*mu0*(lamda**6 - 1)*(lamda**4*(2*lamda**2 - 3) + 1)*exp(m2*q*(lamda**4*(2*lamda**2 - 3) + 1)/lamda**4)/lamda**9
        else:
            print 'Configuration not implemented.'

    if component == 2:
        if config == 'UA':
            return mu0*(lamda + m2*q*(lamda*(lamda**2 - 3) + 2))*(lamda**3 - 1)*exp(m2*q*(lamda*(lamda**2 - 3) + 2)/lamda)/lamda**3
        elif config == 'PS':
            return mu0*(lamda**2 + m2*q*(lamda**2*(lamda**2 - 2) + 1))*(lamda**4 - 1)*exp(m2*q*(lamda**2*(lamda**2 - 2) + 1)/lamda**2)/lamda**5
        elif config == 'EB':
            return mu0*(lamda**4 + m2*q*(lamda**4*(2*lamda**2 - 3) + 1))*(lamda**6 - 1)*exp(m2*q*(lamda**4*(2*lamda**2 - 3) + 1)/lamda**4)/lamda**9
        else:
            print 'Configuration not implemented.'

    print 'Error.'


def objective_function_cau(C, eps_exp, sig_exp, configs, w):

    # assertion for weights
    assert len(w) == 3, 'Set weights of unused configurations to zero!'

    # unpack parameters
    mu0, q, m2 = C

    psi_ua = 0
    psi_ps = 0
    psi_eb = 0

    # set up target function
    if 'UA' in configs:
        psi_ua = stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA']
        psi_ua = (w[0]**2)*dot(psi_ua, psi_ua)

    if 'PS' in configs:
        psi_ps = stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS']
        psi_ps = (w[1]**2)*dot(psi_ps, psi_ps)

    if 'EB' in configs:
        psi_eb = stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB']
        psi_eb = (w[2]**2)*dot(psi_eb, psi_eb)

    # sum targets
    psi = psi_ua + psi_ps + psi_eb

    return psi


def objective_function_pk1(C, eps_exp, sig_exp, configs, w):

    # assertion for weights
    assert len(w) == 3, 'Set weights of unused configurations to zero!'

    # unpack parameters
    mu0, q, m2 = C

    psi_ua = 0
    psi_ps = 0
    psi_eb = 0

    # set up target function
    if 'UA' in configs:
        psi_ua = stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA']
        psi_ua = (w[0]**2)*dot(psi_ua, psi_ua)

    if 'PS' in configs:
        psi_ps = stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS']
        psi_ps = (w[1]**2)*dot(psi_ps, psi_ps)

    if 'EB' in configs:
        psi_eb = stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB']
        psi_eb = (w[2]**2)*dot(psi_eb, psi_eb)

    # sum targets
    psi = psi_ua + psi_ps + psi_eb

    return psi


def jacobian_cau(C, eps_exp, sig_exp, configs, w):

    # assertion for weights
    assert len(w) == 3, 'Set weights w = [..., ..., ...] of unused configurations to zero!'

    # unpack parameters
    mu0, q, m2 = C

    # set up jacobian
    jac = list()
    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_cau(C, eps_exp['UA'], 0, config='UA'), stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_cau(C, eps_exp['PS'], 0, config='PS'), stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_cau(C, eps_exp['EB'], 0, config='EB'), stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_cau(C, eps_exp['UA'], 1, config='UA'), stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_cau(C, eps_exp['PS'], 1, config='PS'), stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_cau(C, eps_exp['EB'], 1, config='EB'), stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_cau(C, eps_exp['UA'], 2, config='UA'), stress_cau(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_cau(C, eps_exp['PS'], 2, config='PS'), stress_cau(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_cau(C, eps_exp['EB'], 2, config='EB'), stress_cau(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    return array(jac)


def jacobian_pk1(C, eps_exp, sig_exp, configs, w):

    # assertion for weights
    assert len(w) == 3, 'Set weights w = [..., ..., ...] of unused configurations to zero!'

    # unpack parameters
    mu0, q, m2 = C

    # set up jacobian
    jac = list()
    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_pk1(C, eps_exp['UA'], 0, config='UA'), stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_pk1(C, eps_exp['PS'], 0, config='PS'), stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_pk1(C, eps_exp['EB'], 0, config='EB'), stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_pk1(C, eps_exp['UA'], 1, config='UA'), stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_pk1(C, eps_exp['PS'], 1, config='PS'), stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_pk1(C, eps_exp['EB'], 1, config='EB'), stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    dpsi = 0

    if 'UA' in configs:
        dpsi += (2*w[0]**2)*dot( d_stress_pk1(C, eps_exp['UA'], 2, config='UA'), stress_pk1(C, eps_exp['UA'], config='UA') - sig_exp['UA'] )
    if 'PS' in configs:
        dpsi += (2*w[1]**2)*dot( d_stress_pk1(C, eps_exp['PS'], 2, config='PS'), stress_pk1(C, eps_exp['PS'], config='PS') - sig_exp['PS'] )
    if 'EB' in configs:
        dpsi += (2*w[2]**2)*dot( d_stress_pk1(C, eps_exp['EB'], 2, config='EB'), stress_pk1(C, eps_exp['EB'], config='EB') - sig_exp['EB'] )

    # append current parameter derivative
    jac.append(dpsi)

    return array(jac)
